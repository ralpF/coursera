#include "union-find.h"




namespace coursera {
    namespace algorithms {

        void QuickFind::Union(int x, int y)
        {
            if (Connected(x, y) == true)
                return;

            int old_indx = arr[x];
            int new_indx = arr[y];
            for (int i = 0; i < len; ++i)
                if (arr[i] == old_indx)
                    arr[i] = new_indx;
        }

        bool QuickFind::Connected(int x, int y)
        {
            return arr[x] == arr[y];
        }
    }
}