#ifndef UNION_FIND_H
#define UNION_FIND_H

#include <stdexcept>


namespace coursera {
    namespace algorithms {

        class UnionFind
        {
        protected:
            int *arr;
            int len;

        public:
            UnionFind(void) = delete;
            UnionFind(int numElem) : arr(new int[numElem]), len(numElem) {
                for (int i = 0; i < numElem; ++i)
                    arr[i] = i;
            }
            virtual ~UnionFind() { delete[] arr; }

            virtual void Union(int x, int y) = 0;
            virtual bool Connected(int x, int y) = 0;
        };


        class QuickFind : public UnionFind
        {
        public:
            QuickFind(void) = delete;
            QuickFind(int numElem) : UnionFind(numElem) {}

            void Union(int x, int y) override;
            bool Connected(int x, int y) override;

        };
    }
}

#endif
